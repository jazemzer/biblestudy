var path = require('path');

var config = require('../config/env/local');
require(path.resolve('../modules/backend/server/models/note.server.model'));
require(path.resolve('../modules/backend/server/models/verse.server.model'));
require(path.resolve('../modules/users/server/models/user.server.model'));
var mongoose = require('mongoose'),
		Note = mongoose.model('Note'),
		User = mongoose.model('User'),
		Verse = mongoose.model('Verse');

mongoose.connect('mongodb://localhost/biblestudy');

// Question.find({_id : '57c082f8b27c3f575f561dcf'}).exec(function(err, doc){
// 	console.log(doc);
// });

// Verse.findOne({ '_id' : 'genesis_1_1' })
// 	.populate('questions').exec(function (err, verse) {
// 		Verse.populate(verse, { path: 'questions.user', model : 'User' }, function(err, doc){
// 			console.log(doc);
// 			process.exit();
// 		});
// });

// Note.find({ category : 'question' }).sort({ created : -1 }).exec(function (err, docs) {
// 	console.log(docs);
// })

var translation = 'translations.' + 'niv';
  	// project[translation] = 1;
Verse.aggregate([
	{
    $match: {
        book: 'genesis',
        chapter :1
    }
  },
  // {
  // 	$unwind : "$crossReferences",
  // },
  // {
  // 	$lookup : {
  // 		from : 'verses',
  // 		localField : 'crossReferences',
  // 		foreignField : '_id',
  // 		as : 'xrefs'
  // 	}
  // },
  // { "$unwind": "$xrefs" },
  // { 
  // 	$group: {
  //       "_id": "$_id",
  //       "crossReferences": { "$push": "$crossReferences" },
  //       "xrefs": { "$push": "$xrefs" }
  //   }
  // },
  // {
  // 	$project : {
  // 		chapter : 1, 
  // 		book : 1,
  // 		verse : 1,
  // 		sortId : 1,
  // 		notes : 1, 
  // 		crossReferences : 1, 
  // 		xrefs : 1,
  // 		text : "$" + translation 		
  // 	}
  // }
	]).exec(function (err, results) {
		console.log(results);
		 // vereses = results.map(function(result) {
   //    return new Verse( result );
   //  });

   //  Employee.populate(results,{ "path": "_id" },function(err,results) {
   //    console.log(results);
   //    callback(err);
   //  });

	process.exit();

});



// var calc = require('./calculator');

// var result = calc.addsync(2, 3);
// console.log(result);

// calc.addasync(1, 3, display);

// function display(message) {
// 	console.log(message);
// }
	
// // Anonymous function sent as callback	
// calc.addasync(1, 3, function(msg) {
// 	console.log(msg);
// });	



