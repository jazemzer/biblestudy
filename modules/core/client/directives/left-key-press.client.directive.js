'use strict';

angular.module('core')
  .directive('leftKeyPress', function ($timeout, $sce, VerseService) {
    return { 
      restrict: 'A',
      link: function (scope, element, attr) {
        angular.element(document.querySelector('body')).on('keydown', function (evt) {
          var $activeElement = angular.element(document.activeElement);
          if($activeElement[0].tagName === 'BODY' && evt.keyCode === 37) {
            element.triggerHandler('click');
          }
        });
      }
    };
  });