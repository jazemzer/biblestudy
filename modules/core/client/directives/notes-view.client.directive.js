'use strict';

angular.module('core')
  .directive('notesView', function ($timeout, $sce, VerseService) {
    return { 
      restrict: 'E',
      scope : {
        verse:"=",
        isCreator:"@" // Note the capitalization causes the usage to be is-creator
      },
      templateUrl: 'templates/notes-view.html',
      link: function (scope, element, attr) {
        
      }, 
      controller : function ($scope) {        
        $scope.deleteNote = function (verse, note) {
          VerseService.deleteNote(verse._id, note._id).then(function(data){
            // Update data after save.
            for(var i = 0; i < verse.notes.length; i++){
              if(verse.notes[i]._id === note._id){
                break;
              }
            }
            verse.notes.splice(i,1);
          });
        };
        $scope.trustNoteHtml = function (note) {
          return $sce.trustAsHtml(note.text.replace(/\n/g, '<br/>'));
        };
      }
    };
  });