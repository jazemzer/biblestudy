'use strict';

angular.module('core')
  .directive('referencesView', function ($timeout, $sce, VerseService) {
    return { 
      restrict: 'E',
      scope : {
        verse:"=",
        isOpen:"@"
      },
      templateUrl: 'templates/references-view.html',
      link: function (scope, element, attr) {
        
      }, 
      controller : function ($scope) {        
        
      }
    };
  });