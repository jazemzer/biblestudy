'use strict';

angular.module('core')
  .directive('noteForm', function ($timeout, $sce, VerseService, PreferencesService) {
    return { 
      restrict: 'E',
      scope : {
        verse:'=',
        form:'=',
        index:'@',
        openReferences:'@',
        isCreator:'@'
      },
      templateUrl: 'templates/note-form.html',
      link: function (scope, element, attr) {
        
      }, 
      compile: function(element, attrs){
        if (!attrs.openReferences) {
          attrs.openReferences = false;
        }
      },
      controller : function ($scope) {
        $scope.init_form = function () {
          $scope.verse.category = 'question';
          $scope.verse.tags = [];
        };
        $scope.verse.matching_verses = [];
        $scope.addNewReference = function (verse) {
          // console.log(verse.newReference);
          VerseService.findVerse(verse.newReference).then(function(data){
            $scope.verse.matching_books = data.matching_books;
            if(data.matching_verses !== undefined) {
              for(var i = 0; i < data.matching_verses.length; i++) {
                $scope.verse.matching_verses.push(data.matching_verses[i]);
              }
            }
          });
        };
        $scope.selected = [];
        $scope.toggle = function (item, list) {
          var idx = list.indexOf(item);
          if (idx > -1) {
            list.splice(idx, 1);
          }
          else {
            list.push(item);
          }
        };
        $scope.exists = function (item, list) {
          return list.indexOf(item) > -1;
        };
        $scope.addNote = function (verse) {
          var payload = {
            text : verse.note,
            category : verse.category,
            tags: verse.tags,
            verses : $scope.selected
          };
          VerseService.addNote(verse._id, payload).then(function(data){
            // console.log(data);
            verse.notes = data.notes;
          });

          // Fire and forget
          var data = {
            lastVisit : {
              book : verse.book,
              chapter : verse.chapter,
              verseId : verse._id
            },
          };  
          PreferencesService.saveLastVisitInfo(data);
        };        
      }
    };
  });