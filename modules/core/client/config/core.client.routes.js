'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

    // Redirect to 404 when route not found
    $urlRouterProvider.otherwise(function ($injector, $location) {
      $injector.get('$state').transitionTo('not-found', null, {
        location: false
      });
    });

    // Home state routing
    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'modules/core/client/views/home.client.view.html'
    })
    .state('chapter', {
      url: '/read/:book/:chapter',
      templateUrl: 'modules/core/client/views/chapter.client.view.html'
    })
    .state('feed', {
      url: '/feed',
      templateUrl: 'modules/core/client/views/feed.client.view.html'
    })
    .state('verse', {
      url: '/verse/:verseID',
      templateUrl: 'modules/core/client/views/verse.client.view.html'
    })
    .state('preferences', {
      url: '/preferences',
      templateUrl: 'modules/core/client/views/preferences.client.view.html'
    })
    .state('not-found', {
      url: '/not-found',
      templateUrl: 'modules/core/client/views/404.client.view.html',
      data: {
        ignoreState: true
      }
    })
    .state('bad-request', {
      url: '/bad-request',
      templateUrl: 'modules/core/client/views/400.client.view.html',
      data: {
        ignoreState: true
      }
    })
    .state('forbidden', {
      url: '/forbidden',
      templateUrl: 'modules/core/client/views/403.client.view.html',
      data: {
        ignoreState: true
      }
    });
  }
]);
