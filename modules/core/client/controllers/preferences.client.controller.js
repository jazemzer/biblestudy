'use strict';

angular.module('core').controller('PreferencesController', ['$scope', 'Authentication',
 '$mdSidenav', '$mdBottomSheet', '$mdMedia', '$mdDialog', '$log', '$state', '$sce', '$mdToast',
 '$stateParams', '$location', '$anchorScroll', 'VerseService', 'PreferencesService',
function($scope, Authentication, $mdSidenav, $mdBottomSheet, $mdMedia, $mdDialog, $log, $state, $sce, $mdToast,
  $stateParams, $location, $anchorScroll, VerseService, PreferencesService) {

  $scope.authentication = Authentication;
  var user = Authentication.user;
  var self = this;

  if(user === undefined || user === '') {
    $state.go('authentication.signin');
  }
  else {
    self.defaultTranslation = 'niv'; // Default selection
    PreferencesService.fetchPreferences().then(function (data) {
      if(data.defaultTranslation !== undefined) {
        self.defaultTranslation = data.defaultTranslation;
      }
      if(data.otherTranslations !== undefined) {
        self.selected = data.otherTranslations;
      }
    });
  }

  self.translations = ['niv','tamil','ampc','nlt','esv','nasb','kjv','hcsb','isv','net','gwt','jps','nasb77','jub','kj2000','akjv','asv','drb','dbt','erv','wbt','web','ylt'];

  self.selected = [];
  self.toggle = function (item, list) {
    var idx = list.indexOf(item);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(item);
    }
  };
  self.exists = function (item, list) {
    return list.indexOf(item) > -1;
  };

  self.savePreferences = function () {
    // Remove the default translation if its preselected
    var idx = self.selected.indexOf(self.defaultTranslation);
    if (idx > -1){
      self.selected.splice(idx, 1);
    }
    var payload = {
      defaultTranslation : self.defaultTranslation,
      otherTranslations : self.selected,
    };
    PreferencesService.savePreferences(payload).then(function(data){
      $mdToast.show(
        $mdToast.simple()
          .textContent('Save successful!')
          .position("bottom right")
          .hideDelay(3000)
      );
    });
  };
}]);

