'use strict';

angular.module('core').controller('ChapterController', ['$scope', 'Authentication',
 '$mdSidenav', '$mdBottomSheet', '$mdMedia', '$mdDialog', '$log', '$state', '$sce', '$timeout',
 '$filter', '$stateParams', '$location', '$anchorScroll', 'VerseService', 'PreferencesService', 
function($scope, Authentication, $mdSidenav, $mdBottomSheet, $mdMedia, $mdDialog, $log, $state, $sce, $timeout,
  $filter, $stateParams, $location, $anchorScroll, VerseService, PreferencesService) {

  $scope.authentication = Authentication;
  var user = Authentication.user;
  var self = this;  

  if($location.hash() === undefined || $location.hash() === ''){
    $location.hash('pagetop');
  } else {
    self.highlightVerse = $location.hash();
  }

  $scope.$on('verseRenderFinished', function(event) {
    $anchorScroll();
  });

  self.fetchVerses = function fetchVerses(book, chapter, toggle) {
    self.chosenBook = book;
    self.chosenChapter = chapter;
    VerseService.getVerses(book, chapter).then(function(content){
      self.verses = content;

      if(toggle){
        $mdSidenav('left').toggle();
      }
    });
  };

  if(user === undefined || user === '') {
    $state.go('authentication.signin');
  }
  else{  
    self.isCreator = user.roles.indexOf('creator') > 0 ? true : false;
    VerseService.getTOC().then(function(data){
      self.toc = data;
      // Search for the book in TOC and select it - so as to allow population of chapters
      self.selected_book = $filter('filter')(data, { book : $stateParams.book })[0];
    });

    self.fetchVerses($stateParams.book, parseInt($stateParams.chapter));
  }
    
  self.selectBook = function selectBook(book) {
    self.selected_book = book;
    self.selectChapter(1);
    self.book_clicked = true;
  };

  self.selectChapter = function selectChapter(chapter, toggle){
    self.fetchVerses(self.selected_book.book, chapter, toggle);
  };  

  self.refreshVerse = function refreshVerse(verseID) {
    VerseService.fetchVerse(verseID).then(function(data){
      // Figure out a way to use this info
    });
  };  

}]);

