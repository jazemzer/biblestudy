'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication',
 '$mdSidenav', '$mdBottomSheet', '$mdMedia', '$mdDialog', '$log', '$state', '$sce',
 '$stateParams', '$location', '$anchorScroll', 'VerseService', 'PreferencesService', 
function($scope, Authentication, $mdSidenav, $mdBottomSheet, $mdMedia, $mdDialog, $log, $state, $sce,
  $stateParams, $location, $anchorScroll, VerseService, PreferencesService) {

  $scope.authentication = Authentication;
  var user = Authentication.user;
  var self = this;  

  if(user === undefined || user === '') {
    $state.go('authentication.signin');
  }
  else{
    PreferencesService.fetchPreferences().then(function (preference) {
      if (preference !== undefined && preference.lastVisit !== undefined){
        var lastVisit = preference.lastVisit;
        $state.go('verse', { verseID : lastVisit.verseId });
        // $state.go('chapter', { book: lastVisit.book, chapter : lastVisit.chapter, '#' : lastVisit.verseId });
      } else {
        $state.go('chapter', { book: 'genesis', chapter : 1 });
      }
    });
  }

}]);

