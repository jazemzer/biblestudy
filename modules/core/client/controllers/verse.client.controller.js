'use strict';

angular.module('core').controller('VerseController', ['$scope', 'Authentication',
 '$mdSidenav', '$mdBottomSheet', '$mdMedia', '$mdDialog', '$log', '$state', '$sce',
 '$stateParams', '$location', '$anchorScroll', 'VerseService',
function($scope, Authentication, $mdSidenav, $mdBottomSheet, $mdMedia, $mdDialog, $log, $state, $sce,
  $stateParams, $location, $anchorScroll, VerseService) {

  $scope.authentication = Authentication;
  var user = Authentication.user;
  var self = this;  

  if(user === undefined || user === '') {
    $state.go('authentication.signin');
  }
  else{
    self.user = user;
    self.isCreator = user.roles.indexOf('creator') > 0 ? true : false;
    VerseService.fetchVerse($stateParams.verseID).then(function(data){
      self.verse = data;
      self.verse.category = 'question';
      self.verse.tags = [];    
    });
  }

  self.trustNoteHtml = function trustNoteHtml(note) {
    return $sce.trustAsHtml(note.text.replace(/\n/g, '<br/>'));
  };

  self.filterTranslations = function(translations, user, type) {
    var preference = user.preference;
    var temp = angular.copy(translations);

    if(preference.defaultTranslation) {
      delete temp[preference.defaultTranslation];
    }

    var preferedTranslations = preference.otherTranslations;
    if(preferedTranslations !== undefined) {      
      for(var key in translations) {
        if(type === 'preferred' && preferedTranslations.indexOf(key) < 0) {
          delete temp[key];
        }
        else if(type === 'other' && preferedTranslations.indexOf(key) >= 0) {
          delete temp[key];
        }
      }
    }
    return temp;
  };

  self.navigatePrev = function () {
    if (self.verse.previousVerseId) {
      $state.go('verse', { verseID: self.verse.previousVerseId });
    }
  };
  self.navigateNext = function () {
    if (self.verse.nextVerseId) {
      $state.go('verse', { verseID: self.verse.nextVerseId });
    }
  };
  
}]);

