'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$window', '$location', '$rootScope', '$state', '$mdSidenav', 'Authentication', 'Menus',
  function ($scope, $window, $location, $rootScope, $state, $mdSidenav, Authentication, Menus) {
    
    var backbuttonStates = [];
    var previousState;
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){ 
      var currentState = $state.current.name;
      previousState = fromState;
      // Hide or show backbutton
      if(backbuttonStates.indexOf(currentState) >= 0) {
        $scope.showBackButton = true;
      }
      else {
        $scope.showBackButton = false;
      }
    });


    var originatorEv;
    var self = this;

    self.toggleList = function toggleLeftPane() {
      $mdSidenav('left').toggle();
    };

    self.goHome = function(){
      // $location.path('/');
      $window.location.href = '/';
    };
    self.goBack = function() {
      $state.go(previousState);
    };
    
    self.openMenu = function($mdOpenMenu, ev) {
      originatorEv = ev;
      $mdOpenMenu(ev);
    };

    // Expose view variables
    $scope.$state = $state;
    $scope.authentication = Authentication;

    // Get the topbar menu
    $scope.menu = Menus.getMenu('topbar');

    // Get the account menu
    // $scope.accountMenu = Menus.getMenu('account').items[0];

    // Toggle the menu items
    $scope.isCollapsed = false;
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };

    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = false;
    });


  }
]);
