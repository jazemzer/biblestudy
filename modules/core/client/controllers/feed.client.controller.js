'use strict';

angular.module('core').controller('FeedController', ['$scope', 'Authentication',
 '$mdSidenav', '$mdBottomSheet', '$mdMedia', '$mdDialog', '$log', '$state', '$sce',
 '$location', '$anchorScroll', 'FeedService',
function($scope, Authentication, $mdSidenav, $mdBottomSheet, $mdMedia, $mdDialog, $log, $state, $sce,
  $location, $anchorScroll, FeedService) {

  $scope.authentication = Authentication;
  var user = Authentication.user;
  var self = this;

  FeedService.fetchLatest().then(function (data) {
    self.notes = data;
  });

}]);

