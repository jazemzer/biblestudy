(function () {
  'use strict';

  angular
    .module('core')
    .factory('PreferencesService', PreferencesService);

  PreferencesService.$inject = ['$http', '$q'];

  function PreferencesService($http, $q) {

    return {      
      saveLastVisitInfo : saveLastVisitInfo,  
      fetchPreferences : fetchPreferences,
      savePreferences : savePreferences, 
    };
    
    function fetchPreferences(){
      var deferred = $q.defer();
      $http.get('api/preferences/').then(function(result) {
        deferred.resolve(result.data);
      }, function(result) {
        deferred.reject(result);
      });

      return deferred.promise;
    }

    function saveLastVisitInfo(data) {
      var deferred = $q.defer();
      $http.post('api/preferences/', data).then(function(result) {
        // console.log(result);
        deferred.resolve(result.data);
      }, function(result) {
        deferred.reject(result);
      });
      return deferred.promise;
    }

    function savePreferences(data) {
      var deferred = $q.defer();
      $http.post('api/preferences/', data).then(function(result) {
        // console.log(result);
        deferred.resolve(result.data);
      }, function(result) {
        deferred.reject(result);
      });
      return deferred.promise;
    }    
    
  }
})();

