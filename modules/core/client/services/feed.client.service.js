(function () {
  'use strict';

  angular
    .module('core')
    .factory('FeedService', FeedService);

  FeedService.$inject = ['$http', '$q'];

  function FeedService($http, $q) {

    return {      
      fetchLatest : fetchLatest,    
    };    
    
    function fetchLatest(){
      var deferred = $q.defer();
      $http.get('api/notes').then(function(result) {
        deferred.resolve(result.data);
      }, function(result) {
        deferred.reject(result);
      });

      return deferred.promise;
    }

    // function saveLastVisitInfo(data) {
    //   var deferred = $q.defer();
    //   $http.post('api/preferences/', data).then(function(result) {
    //     // console.log(result);
    //     deferred.resolve(result.data);
    //   }, function(result) {
    //     deferred.reject(result);
    //   });
    //   return deferred.promise;
    // }    
    
  }
})();

