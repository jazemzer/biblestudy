(function () {
  'use strict';

  angular
    .module('core')
    .factory('VerseService', VerseService);

  VerseService.$inject = ['$http', '$q'];

  function VerseService($http, $q) {

    return {      
      getTOC : getTOC,      
      getVerses : getVerses,
      addNote : addNote,
      deleteNote : deleteNote,
      fetchVerse : fetchVerse,
      findVerse : findVerse,
    };
    function getTOC(){
      var deferred = $q.defer();
      $http.get('api/tableofcontents').then(function(result) {
        deferred.resolve(result.data);
      }, function(result) {
        deferred.reject(result);
      });

      return deferred.promise;
    }

    function getVerses(book, chapter){
      var deferred = $q.defer();
      $http.get('api/book/'+book+'/chapter/'+chapter+'/verses').then(function(result) {
        deferred.resolve(result.data);
      }, function(result) {
        deferred.reject(result);
      });

      return deferred.promise;
    }

    function findVerse(text){
      var deferred = $q.defer();
      $http.get('api/verses/find/'+text).then(function(result) {
        deferred.resolve(result.data);
      }, function(result) {
        deferred.reject(result);
      });

      return deferred.promise;
    }

    function fetchVerse(verseID){
      var deferred = $q.defer();
      $http.get('api/verses/' + verseID).then(function(result) {
        deferred.resolve(result.data);
      }, function(result) {
        deferred.reject(result);
      });

      return deferred.promise;
    }
    
    // function removeConstituencyFromList(constituencyId) {
    //   var deferred = $q.defer();      
    //   $http.delete('api/addtolist/' + constituencyId).then(function(result) {
    //     console.log(result);        
    //     deferred.resolve(result.data);
    //   }, function(result) {
    //     deferred.reject(result);
    //   });

    //   return deferred.promise;
    // }
    
    function addNote(verseID, data) {
      var deferred = $q.defer();
      $http.post('api/verses/' + verseID + '/notes', data).then(function(result) {
        // console.log(result);
        deferred.resolve(result.data);
      }, function(result) {
        deferred.reject(result);
      });
      return deferred.promise;
    }

    function deleteNote(verseID, noteID) {
      var deferred = $q.defer();
      var data = {
        verseID : verseID
      };
      // console.log(data);
      $http.delete('api/notes/' + noteID, { params: data }).then(function(result) {
        // console.log(result);
        deferred.resolve(result.data);
      }, function(result) {
        deferred.reject(result);
      });
      return deferred.promise;
    }
    
  }
})();

