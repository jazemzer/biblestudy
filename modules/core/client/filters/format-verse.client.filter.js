'use strict';

angular.module('core')
  .filter('formatVerse', [function() {
    return function(string) {
      if (!angular.isString(string)) {
        return string;
      }
      var index = string.lastIndexOf('_');
      var tempArr = string.replace(/_/g, ' ').split('');
      tempArr[index] = ':';
      return tempArr.join('');
    };
  }])
  .filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    };
  });