'use strict';

angular.module('core')
  .filter('removeUnderscore', [function() {
    return function(string) {
      if (!angular.isString(string)) {
        return string;
      }
      return string.replace(/_/g, ' ');
    };
  }]);