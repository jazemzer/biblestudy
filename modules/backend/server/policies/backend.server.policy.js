'use strict';

/**
 * Module dependencies.
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Articles Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/verses/:verseID',
      permissions: '*'
    }, {
      resources: '/api/verses/:verseID',
      permissions: '*'
    }, {
      resources: '/api/preferences',
      permissions: ['get', 'post']
    }]
  }, {
    roles :['creator'],
    allows: [{
      resources: '/api/verses/:verseID/notes',
      permissions: ['post']
    }, {
      resources: '/api/notes/:noteID',
      permissions: ['get','put','delete']
    }, {
      resources: '/api/preferences',
      permissions: ['get', 'post']
    }]  
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/tableOfContents',
      permissions: ['get']
    }, {
      resources: '/api/book/:book/chapter/:chapterID/verses',
      permissions: ['get']
    }, {
      resources: '/api/verses/:verseID',
      permissions: ['get']
    }, {
      resources: '/api/verses/find/:verseNotation',
      permissions: ['get']
    }, {
      resources: '/api/preferences',
      permissions: ['get', 'post']
    }, {
      resources: '/api/notes',
      permissions: ['get']
    }]
  }, {
    roles: ['guest'],
    allows: [{
      resources: '/api/tableOfContents',
      permissions: ['get']
    }, {
      resources: '/api/book/:book/chapter/:chapterID/verses',
      permissions: ['get']
    }, {
      resources: '/api/notes',
      permissions: ['get']
    }]
  }]);
};

/**
 * Check If Articles Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];

  // If an article is being processed and the current user created it then allow any manipulation
  if (req.article && req.user && req.article.user.id === req.user.id) {
    return next();
  }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred.
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
