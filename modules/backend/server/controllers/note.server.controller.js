'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  waterfall = require('async-waterfall'),
  Verse = mongoose.model('Verse'),
  Note = mongoose.model('Note'),
  DEFAULT_TRANSLATION = 'niv',
  helper = require('./server.helper'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

exports.addNoteAgainstVerse = function (req, res) {
  var verse = req.verse;
  var body = req.body;

  var note = new Note({
    text : body.text,
    category : body.category,
    tags: body.tags,
    user : req.user,
  });

  // Link related verses to the note
  note.verses.push(verse._id);
  if(body.verses) {
    note.verses = note.verses.concat(body.verses);
  }
  note.save().then(function(result){
    // Update notes for all related verses
    Verse.update(
      { _id: { $in : note.verses } },
      { $addToSet: { notes: note } },
      { multi: true },
      function (err, result) {
        // result contains { ok : 1}
        // Assuming success add note to the current verse for display
        verse.notes.push(note);
        res.json(verse);
      }
    );
  });
};

exports.listNotes = function (req, res) {
  res.json(req.verse.notes);

};

exports.fetchLatest = function (req, res) {
  var preference = req.user.preference;
  var translation = DEFAULT_TRANSLATION;
  if(preference !== undefined && preference.defaultTranslation !== undefined){
    translation = preference.defaultTranslation;
  }
  var category = 'question';
  var limit = 100;
  if(req.query.category){
    category = req.query.category;
    limit = req.query.limit;
  }
  Note.find({ category : 'question' })
  .sort({ created : -1 })
  .populate({ path : 'verses', model : 'Verse', select : 'translations.' + translation })
  .exec(function (err, docs) {
    console.log(docs);
    docs.forEach(function(item){
      helper.map_translation_to_text(item.verses,translation, false);
    });
    res.json(docs);
  });
};

exports.deleteNote = function (req, res) {
  var note = req.note;

  waterfall([
    function(callback){
      Verse.update(
        { _id : { $in : note.verses } },
        { $pull : { notes : note._id } },
        { multi : true },
        function (err, result) {
          console.log(result);
        }  
      );
      // var verseID = req.query ? req.query.verseID : undefined;
      // if(verseID) {
      //   Verse
      //   .findOne({ _id : verseID })
      //   .populate('notes')
      //   .exec(function(err, verse){
      //     if(err) console.log(err);

      //     for(var i = 0; i < verse.notes.length; i++) {
      //       if(verse.notes[i]._id === req.note._id){
      //         break;
      //       }
      //     }
      //     verse.notes.splice(i,1); //Remove the note using its index
      //     // Update the note
      //     verse.save(function(err, doc){
      //       callback(err);
      //     });
      //   });
      // }
      callback(null);
    },
    function(callback) {
      note.remove(function(err, doc){
        if(err) console.log(err);
        callback(err);
      });
    }
  ],function(err, result){
    if (err) {
      res.status(500).send({
        message: 'Server error'
      });
    }
    res.json({ result : 'success' });  
  });
  
};



exports.ByNoteID = function (req, res, next, id) {
  Note.findOne({ '_id' : id }).populate('verses').exec(function (err, note) {
    if (err) {
      return next(err);
    } else if (!note) {
      return res.status(404).send({
        message: 'No note with that identifier has been found'
      });
    }
    req.note = note;
    next();
  });
};
