'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Verse = mongoose.model('Verse'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));



exports.ByBook = function (req, res, next, book) {
  req.book = book;
  next();
};

exports.ByTranslation = function (req, res, next, translation) {
  req.translation = translation;
  next();
};
