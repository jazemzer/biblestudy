'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Verse = mongoose.model('Verse'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

var extend = require('util')._extend;

exports.fetchPreference = function(req, res) {
  res.json(req.user.preference);
};


exports.savePreference = function(req, res) {
  var user = req.user;  
  user.markModified('preference');
  user.preference = extend(user.preference, req.body);
  
  user.save(function(err, doc) {
    res.json(doc.preference);
  });
};
