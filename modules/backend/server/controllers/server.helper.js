'use strict';
function _map_translation_to_text (verses, translation, removeOtherTranslations) {
  // Copy field values from 'translations.<XXX>' to 'text'
  for(var i = 0; i < verses.length; i++){
    var v = verses[i];
    v.text = v.translations[translation];
    if(removeOtherTranslations) {
      v.translations = {}; // Emptying translations to reduce data transfer
    }

    if(v.crossReferences !== undefined) {
      for(var j = 0; j < v.crossReferences.length ; j++){          
        var xref = v.crossReferences[j];
        xref.text = xref.translations[translation];
        // if(removeOtherTranslations) {
        //   xref.translations = {}; // No idea why this doesn't work. It empties 'text' field too
        // }
      }
    }
  }
}

module.exports = {
  map_translation_to_text : _map_translation_to_text
};