'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  waterfall = require('async-waterfall'),
  Verse = mongoose.model('Verse'),
  DEFAULT_TRANSLATION = 'niv',
  helper = require('./server.helper'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

var BOOK_LIST_WO_NUM = ['genesis','exodus','leviticus','numbers','deuteronomy','joshua','judges','ruth','samuel','kings','chronicles','ezra','nehemiah','esther','job','psalms','proverbs','ecclesiastes','songs','isaiah','jeremiah','lamentations','ezekiel','daniel','hosea','joel','amos','obadiah','jonah','micah','nahum','habakkuk','zephaniah','haggai','zechariah','malachi','matthew','mark','luke','john','acts','romans','corinthians','galatians','ephesians','philippians','colossians','thessalonians','timothy','titus','philemon','hebrews','james','peter','jude','revelation'];

/**
 * Show the current chapter
 */
exports.readChapter = function (req, res) {
  // console.log(req.verses);
  res.json(req.verses);
};


/**
 * List of Books
 */
exports.tableOfContents = function (req, res) {

  var query = [
    { $sort : { 'sortId' : 1 } },
    { 
      $project: {
        bookchaptercode: { $substr: [ '$sortId', 0, 5 ] },
        book : 1,
        chapter : 1,
        verse : 1
      }
    },
    { 
      $group : { 
        _id : { 'book' : '$book', 'chapter' : '$chapter', 'bookchaptercode' : '$bookchaptercode' }, 
        verses : { '$push' : '$verse' } 
      } 
    },
    { 
      $project : { 
        bookcode : { $substr: [ '$_id.bookchaptercode', 0, 2 ] },
        _id : 1,
        verses: 1
      }
    },
    { 
      $group : { 
        _id : { 'book' : '$_id.book', 'bookcode' : '$bookcode' }, 
        chapters : { 
          '$push' : { chapter: '$_id.chapter', verses : '$verses' }
        }
      }
    },
    { $sort : { '_id.bookcode' : 1 } },
    { 
      $project : {
        _id : 0,
        book : '$_id.book',
        book_no : '$_id.bookcode',
        chapters : 1
      }
    }
  ];

  Verse.aggregate(query).exec().then(function (docs) {
    // Verse.find().distinct('book', function(error, books) {
    //   res.json({ books : books, toc: docs });
    // });
    res.json(docs);
  });

  
  // res.json({ 'message' : 'success' });
};

exports.findVerse = function(req, res) {
  var verseNotation = req.params.verseNotation.toLowerCase();
  var part_book_chapter_verse_regex = new RegExp("([0-9]*)[ ]*([a-zA-Z]+)[ ]*([0-9]*)[ ]*:{0,1}[ ]*([0-9]*)$");
  var match = part_book_chapter_verse_regex.exec(verseNotation);
  if(match) {
    var part = match[1];
    var book_abbrev = match[2];
    var chapter = match[3];
    var verse = match[4];

    var direct_match = BOOK_LIST_WO_NUM.filter(function(book){
      return book === book_abbrev;
    });

    var books = [];
    var filtered_books;

    if(direct_match.length > 0) {
      books.push(direct_match[0]);
    }
    else {
      filtered_books = BOOK_LIST_WO_NUM.filter(function(book){
        return book[0] === book_abbrev[0];
      });
      if(filtered_books.length === 1) {
        books.push(filtered_books[0]);
      } else {
        for(var i = 0; i < filtered_books.length; i++) {
          var char_matched = 0;
          var curr_book = filtered_books[i];

          // Start from the second character for matching
          var j = 1; 
          var k = 1; 
          while (j < book_abbrev.length && k < curr_book.length) {
            for(; k < curr_book.length; k++){
              if(book_abbrev[j] === curr_book[k]) {
                char_matched++;
                break;
              }
            }
            j++;
          }

          if(char_matched === book_abbrev.length - 1) {
            books.push(curr_book);
          }
        }
      }
    }

    if(books.length > 0) {
      if(chapter !== '' && verse !== '') {
        var verseIds = [];
        for(var index = 0; index < books.length; index++) {
          var verseId = (part !== ''? (part + '_'): '') + books[index] + '_' + chapter + '_' + verse;
          verseIds.push(verseId);
        }
        Verse.find({ _id : { $in : verseIds } })
          .exec(function(err, verses) {
            if(verses.length > 0){
              res.json({
                matching_verses : verses,
              });
            }
            else {
              res.json({ result : "No such verse " });
            }
          });
      }
      else {
        //TODO include part and filter
        res.json({
          book : part + ' ' + book_abbrev, 
          chapter : chapter,
          verse : verse,
          matching_books : books,
        });
      }
    } else {
      res.json({ result : 'No such book' });
    }
  } else {
    res.json({ result : 'Unable to parse verse notation' });
  }
};

exports.fetchVerse = function(req, res) {
  waterfall([
    function(callback){
      // Populate next verse
      Verse.find({ sortId: { $gt : req.verse.sortId } })
        .sort('sortId')
        .limit(1)
        .exec(function(err, verses) {
          if(verses.length > 0){
            // Not the last verse in the bible
            req.verse.nextVerseId = verses[0]._id;
          }
          callback(err);
        });
    },
    function(callback) {
      // Populate previous verse
      Verse.find({ sortId: { $lt : req.verse.sortId } })
        .sort({ sortId : -1 })
        .limit(1)
        .exec(function(err, verses) {
          if(verses.length > 0){
            // Not the first verse in the bible
            req.verse.previousVerseId = verses[0]._id;
          }
          callback(err);
        });
    }
  ],function(err, result){
    if (err) {
      res.status(500).send({
        message: 'Server error'
      });
    }
    res.json(req.verse);
  });
};

/**
 * Verse middleware
 */
exports.versesByChapterID = function (req, res, next, id) {

  // if (!mongoose.Types.ObjectId.isValid(id)) {
  //   return res.status(400).send({
  //     message: 'Verse is invalid'
  //   });
  // }
  var preference = req.user.preference;
  var translation = DEFAULT_TRANSLATION;
  if(preference !== undefined && preference.defaultTranslation !== undefined){
    translation = preference.defaultTranslation;
  }
  var filterQuery = { 'book' : req.book, 'chapter' : id };
  var selectQuery = 'chapter book verse sortId notes crossReferences translations.' + translation;
  
  Verse.find(filterQuery)
    .sort('sortId')
    .populate([
      { path: 'notes',
        model : 'Note',
        populate : {
          path :'user',
          model : 'User',
          select : 'displayName email lastName firstName'
        }
      }, {
        path : 'crossReferences',
        model : 'Verse',
        select : 'book chapter sortId translations.' + translation
      }])
    .select(selectQuery)
    .exec(function (err, verses) {
      if (err) {
        console.log(err);
        return next(err);
      } else if (!verses) {
        return res.status(404).send({
          message: 'No chapter with that identifier has been found'
        });
      }

      helper.map_translation_to_text(verses, translation, true);
      
      // console.log(verses);
      req.verses = verses;
      next();
    });
};



exports.ByVerseID = function (req, res, next, id) {
  var preference = req.user.preference;
  var translation = DEFAULT_TRANSLATION;
  if(preference !== undefined && preference.defaultTranslation !== undefined){
    translation = preference.defaultTranslation;
  }
  Verse.findOne({ '_id' : id })
    .populate([
      { path: 'notes',
        model : 'Note',
        populate : {
          path :'user',
          model : 'User',
          select : 'displayName email lastName firstName'
        }
      },{
        path : 'crossReferences',
        model : 'Verse',
        select : 'book chapter sortId translations.' + translation
      }])
    .exec(function (err, verse) {
      if (err) {
        return next(err);
      } else if (!verse) {
        return res.status(404).send({
          message: 'No verse with that identifier has been found'
        });
      }
      helper.map_translation_to_text([verse],translation, false);
      req.verse = verse;
      next();
    });
};

var BOOK_ABBREV = {
  gen : 'genesis',
  ex : 'exodus',
  lev : 'leviticus',
  num : 'numbers',
  deut : 'deuteronomy',
  josh : 'joshua',
  judg : 'judges',
  ruth : 'ruth',
  sam : 'samuel',
  kings : 'kings',
  chr : 'chronicles',
  ezra : 'ezra',
  neh : 'nehemiah',
  esth : 'esther',
  job : 'job',
  ps : 'psalms',
  prov : 'proverbs',
  ecc : 'ecclesiastes',
  eccl : 'ecclesiastes',
  song : 'songs',
  songs : 'songs',
  isa : 'isaiah',
  jer : 'jeremiah',
  lam : 'lamentations',
  ezek : 'ezekiel',
  dan : 'daniel',
  hos : 'hosea',
  joel : 'joel',
  am : 'amos',
  oba : 'obadiah',
  jon : 'jonah',
  mic : 'micah',
  nah : 'nahum',
  hab : 'habakkuk',
  zeph : 'zephaniah',
  hag : 'haggai',
  zech : 'zechariah',
  mal : 'malachi',
  mt : 'matthew',
  mk : 'mark',
  lk : 'luke',
  jn : 'john',
  acts : 'acts',
  rom : 'romans',
  cor : 'corinthians',
  gal : 'galatians',
  eph : 'ephesians',
  phil : 'philippians',
  col : 'colossians',
  thess : 'thessalonians',
  tim : 'timothy',
  titus : 'titus',
  phile : 'philemon',
  heb : 'hebrews',
  jas : 'james',
  pet : 'peter',
  jude : 'jude',
  rev : 'revelation'
};