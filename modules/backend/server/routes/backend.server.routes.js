'use strict';

/**
 * Module dependencies.
 */
var backendPolicy = require('../policies/backend.server.policy'),
  backend = require('../controllers/backend.server.controller'),
  verses = require('../controllers/verse.server.controller'),
  preferences = require('../controllers/preferences.server.controller'),
  note = require('../controllers/note.server.controller');

module.exports = function (app) {
  app.route('/api/tableOfContents').all(backendPolicy.isAllowed)
    .get(verses.tableOfContents);

  app.route('/api/book/:book/chapter/:chapterID/verses').all(backendPolicy.isAllowed)
    .get(verses.readChapter);

  app.route('/api/verses/:verseID').all(backendPolicy.isAllowed)
    .get(verses.fetchVerse);
  app.route('/api/verses/find/:verseNotation').all(backendPolicy.isAllowed)
    .get(verses.findVerse);

  app.route('/api/preferences').all(backendPolicy.isAllowed)
    .get(preferences.fetchPreference)
    .post(preferences.savePreference);

  app.route('/api/verses/:verseID/notes').all(backendPolicy.isAllowed)
    .post(note.addNoteAgainstVerse);

  // Order of /api/notes is important here
  app.route('/api/notes').all(backendPolicy.isAllowed)
    .get(note.fetchLatest);

  app.route('/api/notes/:noteID').all(backendPolicy.isAllowed)
    .delete(note.deleteNote);

  app.param('noteID', note.ByNoteID);
  app.param('verseID', verses.ByVerseID);
  app.param('chapterID', verses.versesByChapterID);
  app.param('translation', backend.ByTranslation);
  app.param('book', backend.ByBook);
};
