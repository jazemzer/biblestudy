'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var NoteSchema = new Schema({
  text : {
    type : String
  },
  category : {
    type : String,
    enum : ['general', 'prayer', 'question', 'learning'],
    default : 'question'
  },
  created: {
    type: Date,
    default: Date.now
  },
  tags: [{ type: String }],
  verses: [{ type: String, ref: 'Verse' }],
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }

});

// NoteSchema.post('save', function() {
//   console.log('post save fired for note ' + this._id);
// });

mongoose.model('Note', NoteSchema);
