'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var VerseSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  _id: {
    type: String,
    default: '',
  },
  sortId: {
    type: String,
    default: '',
  },
  book: {
    type: String,
    default: ''
  },
  chapter : {
    type: Number,
    min: 1,
    max: 150
  },
  verse : {
    type: Number,
    min: 1,
    max: 180
  },
  //A temporary field into which the actual translation gets copied
  text : {
    type: String
  },
  previousVerseId : {
    type: String
  },
  nextVerseId : {
    type: String
  },
  crossReferences: [{ type: String, ref: 'Verse' }],
  notes: [{ type: Schema.Types.ObjectId, ref: 'Note' }],
  translations : {
    niv : {
      type: String
    },
    tamil : {
      type: String
    },
    ampc : {
      type: String
    },
    htb : {
      type: String
    },
    nlt : {
      type: String
    },
    esv : {
      type: String
    },
    nasb : {
      type: String
    },
    kjv : {
      type: String
    },
    hcsb : {
      type: String
    },
    isv : {
      type: String
    },
    net : {
      type: String
    },
    gwt : {
      type: String
    },
    jps : {
      type: String
    },
    nasb77 : {
      type: String
    },
    jub : {
      type: String
    },
    kj2000 : {
      type: String
    },
    akjv : {
      type: String
    },
    asv : {
      type: String
    },
    drb : {
      type: String
    },
    dbt : {
      type: String
    },
    erv : {
      type: String
    },
    wbt : {
      type: String
    },
    web : {
      type: String
    },
    ylt : {
      type: String
    }
  }
});

module.exports = mongoose.model('Verse', VerseSchema);
