var request = require('request');
var xpath = require('xpath');
var dom = require('xmldom').DOMParser;
var cheerio = require('cheerio');
var logger = require('./log');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/biblestudy');
var db = mongoose.connection

var BibleVerse = require('./models/Verse');
var LowerCasedBooksOfBible = require('./models/Books');
var Utils = require('./Utils/Utils');

// This translation is just to fetch the verse pages
var TRANSLATION = "niv";

// Example : http://biblehub.com/niv/genesis/1.htm
var BOOK_CHAPTER_REGEX = new RegExp(TRANSLATION + "\/(.*)\/([0-9]*).htm");

// Example : http://biblehub.com/genesis/1-31.htm
var VERSE_REGEX = new RegExp("-([0-9]*).htm");

// Example : <a href="/drb/genesis/1.htm">Douay-Rheims Bible</a>
var TRANSLATION_REGEX = new RegExp(".*\/(.*)\/.*\/");

var startUrl = "http://biblehub.com/" + TRANSLATION + "/songs/1.htm";
fetchVerses(startUrl, true)

// LowerCasedBooksOfBible.forEach(function(item, index, theArray) {
//     // console.log(item)
//     BibleVerse.find({book: item}, function(err, bookCollection) {
//         bookCollection.forEach(function(doc){
//             doc.sortOrder = code;
//             doc.save()
//             console.log(doc._id, code)
//         })
//     })    
// })


function process(book, chapter, verseUrl) {

	var match = VERSE_REGEX.exec(verseUrl)
	var verse = 0
	if (match) {
		verse = match[1]
	}	

	request(verseUrl, function(error, response, html) {
		
        if (!error) {
			$ = cheerio.load(html)

            // <span class="versiontext">
            //     <a href="/niv/genesis/1.htm">New International Version</a>
            // </span>
            // <br>
            // God saw all that he had made, and it was very good. And there was evening, and there was morning--the sixth day.
            // <span class="versiontext"> 
            // ...
			var verses = $('.versiontext').map(function() {
                var translationUrl = $(this).find('a').prop('href')
                var match = TRANSLATION_REGEX.exec(translationUrl)
                var translation;
                if (match) { 
                    translation = match[1] 
                }

                // Verses are without enclosing tag as seen in example above
                var verseText = this.nextSibling.nextSibling.nodeValue

                return {
                    translation : translation,
                    verseText : verseText
                }
            }).toArray()

            // Converts to a JSON as below
            // { 
            //     niv: 'And there was evening, and there was morning--the third day.',
            //     nlt: 'And evening passed and morning came, marking the third day.',
            //     ...
            // }
            var translations = {}
            verses.forEach(function(item) {
                translations[item.translation] = item.verseText
            })

            storeVerse(book, chapter, verse, translations)

		} else {
            logger.error("verse: ", error)
        }

	})
}

function storeVerse(book, chapter, verse, translations) {
    // console.log(book, chapter, verse)
    // console.log(translations)
    
    var index = LowerCasedBooksOfBible.indexOf(book)
    var sortOrder = Utils.padDigits(index + 1, 2) + Utils.padDigits(chapter, 3) + Utils.padDigits (verse, 3)
    // console.log(book, chapter, verse, sortOrder);

    var bibleVerse = new BibleVerse(
        { 
            _id: book + '_' + chapter + '_' + verse, 
            book: book, 
            chapter : chapter, 
            verse : verse,
            translations: translations,
            sortId : sortOrder
        });

    bibleVerse.save(function (err) {
        if(err && err.code != 11000) {
            console.log(err)
        }
    })
}

function fetchVerses(nextUrl, flag) {

    if(!flag && nextUrl == startUrl) {
        console.log("Reached the end ");
        return   
    }
    console.log(nextUrl)

    request(nextUrl, function (error, response, html) {
    	if (!error) {
    		var requestUrl = response.request.uri.href;
    		var match = BOOK_CHAPTER_REGEX.exec(requestUrl)
    		$ = cheerio.load(html)
            var book = ""
            var chapter = 0

    		if (match) {
    			book = match[1]
    			chapter = match[2]

    			var verses = $('span[class=reftext] a').map(function(){
    				return $(this).prop('href')
    			}).toArray()
                // console.log(verses[1])


                BibleVerse.count({book: book, chapter: chapter}, function(err, count) {
                    // Process again only if the verse count mismatches                    
                    if(!err) {
                        if (count != verses.length) {
                            console.log("Missing verses for ", book, chapter, "existing : ", count, "total :", verses.length)
                            // console.log(verses);

                            verses.forEach(function (verseUrl) {
                                process(book, chapter, verseUrl)
                            })
                        }
                    }
                    else {
                        logger.error("Error while fetching data for ", book, chapter)
                    }
                });

                // Sample HTML
                // <div id="topheading">
                //     <a href="../revelation/22.htm" title="Revelation 22">◄</a> Genesis 1 
                //     <a href="../genesis/2.htm" title="Genesis 2">►</a>
                // </div>

        		nextUrl = $('div[id=topheading] a').last().prop('href')
        		nextUrl = "http://biblehub.com/" + TRANSLATION + nextUrl.replace("..","")

                fetchVerses(nextUrl)
            }
    	}
        else {
            logger.error("chapter: ", error)                
        }
    })
    // exit;
}

