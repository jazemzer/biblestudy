
var request = require('request');
var cheerio = require('cheerio');
var bookListUrl = "http://www.greatcommission.com/BibleBookChapters.htm";

request(bookListUrl, function(error, response, html) {
    if (!error) {
        $ = cheerio.load(html)
        var books = $('tr > td:nth-child(1) > b').map(function(){
            return $(this).text()
        }).toArray()

        console.log(books)
    }
})


//http://stackoverflow.com/questions/5086390/jquery-camelcase
function toTitleCase(str) {
    return str.replace(/(?:^|\s)\w/g, function(match) {
        return match.toUpperCase();
    });
}
// Or use css  
// 	text-transform: capitalize;