var path = require('path');

var config = require('../config/env/local');
require(path.resolve('../modules/backend/server/models/note.server.model'));
require(path.resolve('../modules/backend/server/models/verse.server.model'));
require(path.resolve('../modules/users/server/models/user.server.model'));
var mongoose = require('mongoose'),
        Note = mongoose.model('Note'),
        User = mongoose.model('User'),
        Verse = mongoose.model('Verse');

mongoose.connect('mongodb://localhost/biblestudy');

var request = require('request');
// request = request.defaults({ pool : { maxSockets: 1 } });

var db = mongoose.connection

var xpath = require('xpath');
var dom = require('xmldom').DOMParser;
var cheerio = require('cheerio');
var logger = require('./log');

var LowerCasedBooksOfBible = require('./models/Books');
var Utils = require('./Utils/Utils');

// Example : http://biblehub.com/niv/genesis/1.htm
var BOOK_CHAPTER_REGEX = new RegExp("Book=(.*)\&Chapter=([0-9]*)");
var BOOK_CHAPTER_VERSE_REGEX = new RegExp("\/(.*)\/([0-9]*)-([0-9]*).htm")

// Example : http://biblehub.com/genesis/1-31.htm
var VERSE_REGEX = new RegExp("-([0-9]*).htm");

// Example : <a href="/drb/genesis/1.htm">Douay-Rheims Bible</a>
var TRANSLATION_REGEX = new RegExp(".*\/(.*)\/.*\/");

// Books with just one chapter
// fetchVerses('http://www.tamil-bible.com/lookup.php?Book=Philemon&Chapter=1');
// fetchVerses('http://www.tamil-bible.com/lookup.php?Book=II_John&Chapter=1');
// fetchVerses('http://www.tamil-bible.com/lookup.php?Book=III_John&Chapter=1');
// fetchVerses('http://www.tamil-bible.com/lookup.php?Book=Jude&Chapter=1');
// fetchVerses('http://www.tamil-bible.com/lookup.php?Book=Obadiah&Chapter=1');

// fetchBooks();
fetchChapters('http://www.tamil-bible.com/chapters.php?Type=Zechariah');
function fetchBooks() {
    request('http://www.tamil-bible.com/tabletype.php?Type=TML', function (error, response, html) {
        if (!error) {
            var requestUrl = response.request.uri.href;
            $ = cheerio.load(html)

            var books = $('a[class=varisai]').map(function(){
                return $(this).prop('href');
            }).toArray();

            books.forEach(function (bookUrl) {
                fetchChapters(bookUrl)
            })                
        }
    })
}

function fetchChapters(bookUrl) {

    request(bookUrl, function (error, response, html) {
        if (!error) {
            var requestUrl = response.request.uri.href;
            $ = cheerio.load(html)

            var chapters = $('table a').map(function(){
                return $(this).prop('href')
            }).toArray()
            console.log(chapters);

            if(chapters.length == 0) {
                console.log("missing chapters" + bookUrl);
            }

            chapters.forEach(function (verseUrl) {
                fetchVerses(verseUrl);
            })                
        }
        else {
            console.log(error + ' ' + bookUrl)
        }
    })
}

function fetchVerses(chapterUrl) {

    request(chapterUrl, function (error, response, html) {
        if (!error) {
            var requestUrl = response.request.uri.href;
            var match = BOOK_CHAPTER_REGEX.exec(requestUrl)
            $ = cheerio.load(html)
            var book = ""
            var chapter = 0
            // console.log(match)
            if (match) {
                book = match[1]
                chapter = match[2]

                book = book.replace("III_","3_");
                book = book.replace("II_","2_");
                book = book.replace("I_","1_");
                book = book.replace("Song_of_Solomon","songs");

                var verses = $('ol').text().split("\n");                

                verses.forEach(function (text) {
                    var len = text.length;
                    var onlytext = text.substring(text.indexOf('. ')+2, len - 1);
                    var verse_no = text.substring(0, text.indexOf('. '));
                    // console.log(verse_no);
                    if (text !== undefined && text !== '')  {
                        process(book.toLowerCase() + '_' + chapter + '_' + verse_no, onlytext);
                    }
                })
            }                
        }
        else {
            console.log(error + ' ' + chapterUrl)
        }
    });
}



function process(key, text) {
    // console.log(key + ' ' + text);    
    Verse.findOne({ _id : key}).exec(function (err, doc) {
        doc.translations.tamil = text;
        doc.save(function (err) {
            if(err && err.code != 11000) {
                console.log(key);
                console.log(err)
            }
        });
    });       

    
}


