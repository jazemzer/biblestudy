var request = require('request');
var xpath = require('xpath');
var dom = require('xmldom').DOMParser;
var cheerio = require('cheerio');
var logger = require('./log');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/biblestudy');
var db = mongoose.connection

var VerseToken = require('./models/VerseToken');
var LowerCasedBooksOfBible = require('./models/Books');
var Utils = require('./Utils/Utils');

// console.log(Utils.padDigits(3, 5));

var BOOK_CHAPTER_VERSE_REGEX = new RegExp("\/text\/(.*)\/([0-9]*)-([0-9]*).htm")

var startUrl = "http://biblehub.com/text/genesis/1-1.htm"


var args = process.argv.slice(2);
var direction = 1;
if(args.length > 0){
	startUrl = "http://biblehub.com/text/" + args[0] +".htm"
	direction = args[1]
}
fetchVerses(startUrl, true, direction)


function fetchVerses(nextUrl, flag, forward) {

    if(!flag && nextUrl == startUrl) {
        console.log("Reached the end ");
        return   
    }
    console.log(nextUrl)

	request(nextUrl, function (error, response, html) {
    	if (!error) {
    		var requestUrl = response.request.uri.href;

    		var match = BOOK_CHAPTER_VERSE_REGEX.exec(requestUrl)
    		$ = cheerio.load(html)
            var book = ""
            var chapter = 0
            var verse = 0

    		if (match) {
    			book = match[1]
    			chapter = match[2]
    			verse = match[3]

    			// console.log(book, chapter, verse)

    			var tokens = $('table.maintext tr').map(function(index, element){
    				// console.log(index)
    				var token = {}
    				token.englishText =  $(this).find('td.eng').text()
    				token.strongs =  $(this).find('td.strongsnt span').first().text()
    				token.transliteration =  $(this).find('td.translit').text()
    				var $orig = $(this).find('td.greek2')
    				token.language = 'Greek'
    				if($orig.length == 0) {
    					token.language = 'Hebrew'
    					$orig = $(this).find('td.hebrew2')
    				}
    				token.originalText =  $orig.text()
    				token.pos =  $(this).find('td.pos span.pos').text()
    				token.book = book
    				token.chapter = chapter
    				token.verse = verse	
    				token.verseId = book + '_' + chapter + '_' + verse
    				token._id = token.verseId + '_' + index // Already adjusted for zeroth place by header

    				var bookIndex = LowerCasedBooksOfBible.indexOf(book)
    				var sortOrder = Utils.padDigits(bookIndex + 1, 2) + Utils.padDigits(chapter, 3) 
    						+ Utils.padDigits (verse, 3) + Utils.padDigits(index, 3)
    				token.sortId = sortOrder

    				return token

    			}).toArray();

				tokens = tokens.filter(function(token){ 
    				if(token.englishText == '') {
    					return false
    				}
    				return true
    			})
    			// console.log(tokens);

    			VerseToken.collection.insert(tokens)

    			if (forward > 0) {
	    			nextUrl = $('div[id=topheading] a').last().prop('href')
    			}
    			else {
    				nextUrl = $('div[id=topheading] a').first().prop('href')    				
    			}
        		nextUrl = "http://biblehub.com/text" + nextUrl.replace("..","")

                fetchVerses(nextUrl, false, forward)
            }
    	}
        else {
            logger.error("chapter: ", error)                
        }
    })
}