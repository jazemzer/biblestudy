var request = require('request');
var xpath = require('xpath');
var dom = require('xmldom').DOMParser;
var cheerio = require('cheerio');
var logger = require('./log');
var path = require('path');

require(path.resolve('../modules/backend/server/models/note.server.model'));
require(path.resolve('../modules/backend/server/models/verse.server.model'));
require(path.resolve('../modules/users/server/models/user.server.model'));
var mongoose = require('mongoose'),
        Note = mongoose.model('Note'),
        User = mongoose.model('User'),
        Verse = mongoose.model('Verse');

// mongoose.connect('mongodb://localhost/biblestudy');

// mongodb://<dbuser>:<dbpassword>@ds053006-a0.mlab.com:53006,ds053006-a1.mlab.com:53006/biblestudy?replicaSet=rs-ds053006
var db = mongoose.connection


var LowerCasedBooksOfBible = require('./models/Books');
var Utils = require('./Utils/Utils');

// This translation is just to fetch the verse pages
var TRANSLATION = "AMPC";

// Example : http://biblehub.com/niv/genesis/1.htm
// https://www.biblegateway.com/passage/?search=Genesis+2&version=AMPC
var BOOK_CHAPTER_REGEX = new RegExp("search=(.*)&version=" + TRANSLATION);

// Example : http://biblehub.com/genesis/1-31.htm
var VERSE_REGEX = new RegExp("-([0-9]*).htm");

// Example : <a href="/drb/genesis/1.htm">Douay-Rheims Bible</a>
var TRANSLATION_REGEX = new RegExp(".*\/(.*)\/.*\/");

var startUrl = "http://biblegateway.com//passage/?search=Song%20of%20Solomon+1&version=" + TRANSLATION;
// var startUrl = "https://www.biblegateway.com/passage/?search=Song%20of%20Solomon+4&version=" + TRANSLATION;
fetchVerses(startUrl, true)


function process(key, text) {
    // console.log(key + ' ' + text);
    Verse.findOne({ _id : key}).exec(function (err, doc) {
        doc.markModified('translations');
        doc.translations[TRANSLATION.toLowerCase()] = text;
        doc.save(function (err) {
            if(err && err.code != 11000) {
                console.log(key);
                console.log(err)
            }
        });
    });       

    
}
function fetchVerses(nextUrl, flag) {

    if(!flag && nextUrl == startUrl) {
        console.log("Reached the end ");
        return   
    }
    console.log(nextUrl)

    request(nextUrl, function (error, response, html) {
        if (!error) {
            var requestUrl = response.request.uri.href;
            var match = BOOK_CHAPTER_REGEX.exec(requestUrl)
            $ = cheerio.load(html)

            // Remove subscripts 
            $('sup').remove()
            $('.chapternum').remove()
            
            var book = ""
            var chapter = 0

            if (match) {
                var book_chapter = match[1].split('+')
                book = book_chapter[0]
                chapter = book_chapter[1]

                // Cheerio is not able to filter when there are multiple classes 
                // $('p[class=verse]') fails to give the first verse
                var verses = [];
                $('div[class=passage-text] p').each(function(i, elem){
                    if($(elem).hasClass('verse')) {
                        verses.push($(elem).find('span').text());
                    }
                });
                // console.log(verses)

                book = book.replace(/%20/g,"_")
                book = book.replace("Song_of_Solomon","songs")
                book = book.replace("Psalm","psalms")
                book = book.toLowerCase()
                
                logger.info(chapter, book)

                verses.forEach(function (text, index) {
                    var verse_no = index + 1;
                    process(book + '_' + chapter + '_' + verse_no, text);
                })                

                // // Sample HTML
                // <div class="next-chapter">
                //  <a href="/passage/?search=Song%20of%20Solomon+4&amp;version=AMPC" data-hasqtip="2" oldtitle="Song of Solomon 4" title="" style="margin: 0px; padding-top: 176px; padding-bottom: 748px;">
                //      <span class="icon-passage-next"></span>
                //  </a>
                // </div>

                nextUrl = $('div[class=next-chapter] a').prop('href')
                nextUrl = "http://biblegateway.com/" + nextUrl

                fetchVerses(nextUrl)
            }
        }
        else {
            logger.error("chapter: ", error)                
        }
    })
    // exit;
}

