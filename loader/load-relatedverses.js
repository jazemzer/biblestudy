var path = require('path');

var config = require('../config/env/local');
require(path.resolve('../modules/backend/server/models/note.server.model'));
require(path.resolve('../modules/backend/server/models/verse.server.model'));
require(path.resolve('../modules/users/server/models/user.server.model'));
var mongoose = require('mongoose'),
        Note = mongoose.model('Note'),
        User = mongoose.model('User'),
        Verse = mongoose.model('Verse');

mongoose.connect('mongodb://localhost/biblestudy');

var request = require('request');
var db = mongoose.connection

var xpath = require('xpath');
var dom = require('xmldom').DOMParser;
var cheerio = require('cheerio');
var logger = require('./log');

var LowerCasedBooksOfBible = require('./models/Books');
var Utils = require('./Utils/Utils');

// This translation is just to fetch the verse pages
var TRANSLATION = "niv";

// Example : http://biblehub.com/niv/genesis/1.htm
var BOOK_CHAPTER_REGEX = new RegExp(TRANSLATION + "\/(.*)\/([0-9]*).htm");
var BOOK_CHAPTER_VERSE_REGEX = new RegExp("\/(.*)\/([0-9]*)-([0-9]*).htm")

// Example : http://biblehub.com/genesis/1-31.htm
var VERSE_REGEX = new RegExp("-([0-9]*).htm");

// Example : <a href="/drb/genesis/1.htm">Douay-Rheims Bible</a>
var TRANSLATION_REGEX = new RegExp(".*\/(.*)\/.*\/");

var startUrl = "http://biblehub.com/" + TRANSLATION + "/songs/1.htm";
fetchVerses(startUrl, true)

function process(book, chapter, verseUrl) {

	var match = VERSE_REGEX.exec(verseUrl)
	var verse = 0
	if (match) {
		verse = match[1]
	}	

	request(verseUrl, function(error, response, html) {
		
        if (!error) {
			$ = cheerio.load(html)

			var crossreferences = $('span[class=crossverse] a').map(function() {
                // return $(this).prop('href');
                var refUrl = $(this).prop('href')
                var match = BOOK_CHAPTER_VERSE_REGEX.exec(refUrl)
                // var translation;
                if (match) { 
                    return match[1] + '_' + match[2] + '_' + match[3];
                }
            }).toArray();
            storeVerse(book, chapter, verse, crossreferences)

		} else {
            logger.error("verse: ", error)
        }

	})
}

function storeVerse(book, chapter, verse, crossreferences) {
    // console.log(book, chapter, verse)
    // console.log(translations)
    
    var index = LowerCasedBooksOfBible.indexOf(book)
    var sortOrder = Utils.padDigits(index + 1, 2) + Utils.padDigits(chapter, 3) + Utils.padDigits (verse, 3)
    // console.log(book, chapter, verse, sortOrder);

    Verse.findOne({ _id : book + '_' + chapter + '_' + verse }).exec(function (err, doc) {
        doc.crossReferences = crossreferences;
        doc.save(function (err) {
            if(err && err.code != 11000) {
                console.log(err)
            }
        });
    });       

    
}

function fetchVerses(nextUrl, flag) {

    if(!flag && nextUrl == startUrl) {
        console.log("Reached the end ");
        return   
    }
    console.log(nextUrl)

    request(nextUrl, function (error, response, html) {
    	if (!error) {
    		var requestUrl = response.request.uri.href;
    		var match = BOOK_CHAPTER_REGEX.exec(requestUrl)
    		$ = cheerio.load(html)
            var book = ""
            var chapter = 0

    		if (match) {
    			book = match[1]
    			chapter = match[2]

    			var verses = $('span[class=reftext] a').map(function(){
    				return $(this).prop('href')
    			}).toArray()
                // console.log(verses[1])


                verses.forEach(function (verseUrl) {
                    process(book, chapter, verseUrl)
                })                

                // Sample HTML
                // <div id="topheading">
                //     <a href="../revelation/22.htm" title="Revelation 22">◄</a> Genesis 1 
                //     <a href="../genesis/2.htm" title="Genesis 2">►</a>
                // </div>

        		nextUrl = $('div[id=topheading] a').last().prop('href')
        		nextUrl = "http://biblehub.com/" + TRANSLATION + nextUrl.replace("..","")

                fetchVerses(nextUrl)
            }
    	}
        else {
            logger.error("chapter: ", error)                
        }
    })
    // exit;
}

