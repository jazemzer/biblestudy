'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var VerseTokenSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  _id: {
    type: String,
    default: '',
  },
  verseId: {
    type: String,
    default: '',
  },
  sortId: {
    type: String,
    default: '',
  },
  book: {
    type: String,
    default: ''
  },
  chapter : {
    type: Number,
    min: 1,
    max: 150
  },
  verse : {
    type: Number,
    min: 1,
    max: 180
  },
  pos: {
    type: String,
    default: '',
  },
  strongs: {
    type: String,
    default: '',
  },
  transliteration: {
    type: String,
    default: '',
  },
  language: {
    type: String,
    // http://mongoosejs.com/docs/api.html#schema_string_SchemaString-enum
    enum: {
      values: 'hebrew greek'.split(' '),
      message: 'enum validator failed for path `{PATH}` with value `{VALUE}`'
    },
    default: '',
  },
  originalText: {
    type: String,
    default: '',
  },
  englishText: {
    type: String,
    default: '',
  }, 
});

module.exports = mongoose.model('VerseToken', VerseTokenSchema);
